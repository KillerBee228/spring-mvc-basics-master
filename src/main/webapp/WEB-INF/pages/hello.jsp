<%@ page import="java.util.Collection" %>
<%@ page import="com.springapp.mvc.models.Contacts" %>
<%@ page import="org.springframework.ui.ModelMap" %>
<html>
<body>
	<h1>Hello, contacts, time to add contact!</h1>
	<div class="add">
		<form method="get" action="/phonebook/add">
			<tr>
				<td align="right" valign="top">Name</td>
				<td><input type="text" name="name" size="25"></td>
			</tr>
			<tr>
				<td align="right" valign="top">Phone</td>
				<td><input type="text" name="phone" size="25"></td>
			</tr>
			<tr>
				<td align="right" colspan="2">
					<input type="submit" name="action" value="add">
					<input type="reset" name="reset" value="reset">
				</td>
			</tr>
		</form>
	</div>
	<tr>
		<h3>${users}<h3/>
	<tr/>
	</body>
</html>