package com.springapp.mvc.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SINGERS")
public class Singers implements Serializable {

    private static final long serialVersionUID = -5527566248002296042L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    public Singers() {
    }

    public Singers(int id, String name){
        this.id=id;
        this.name=name;
    }


    public String getName() {
        return name;
    }


    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
