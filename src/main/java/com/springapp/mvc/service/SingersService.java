package com.springapp.mvc.service;

import com.springapp.mvc.models.Singers;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("singerService")
@Transactional
public class SingersService {

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    public List<Singers> getAll() {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM Singers");

        // Retrieve all
        return  query.list();
    }

    /**
     * Retrieves a single person
     */
    public Singers get( Integer id ) {
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Singers singers = (Singers) session.get(Singers.class, id);

        return singers;
    }
    /**
     * Adds a new person
     */
    public void add(Singers singers) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Save
        session.save(singers);
    }

    /**
     * Deletes an existing person
     * @param id the id of the existing person
     */
    public void delete(Integer id) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Singers singers = (Singers) session.get(Singers.class, id);

        // Delete
        session.delete(singers);
    }
}
