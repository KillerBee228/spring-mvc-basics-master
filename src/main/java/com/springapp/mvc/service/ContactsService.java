package com.springapp.mvc.service;

import com.springapp.mvc.models.Contacts;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by aur on 07.04.2015.
 */

@Service("userService")
@Transactional
public class ContactsService {

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    public List<Contacts> getAll() {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM Contacts");

        // Retrieve all
        return  query.list();
    }

    /**
     * Retrieves a single person
     */
    public Contacts get( Integer id ) {
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Contacts contacts = (Contacts) session.get(Contacts.class, id);

        return contacts;
    }
    /**
     * Adds a new person
     */
    public void add(Contacts contacts) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Save
        session.save(contacts);
    }

    /**
     * Deletes an existing person
     * @param id the id of the existing person
     */
    public void delete(Integer id) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Contacts contacts = (Contacts) session.get(Contacts.class, id);

        // Delete
        session.delete(contacts);
    }

}