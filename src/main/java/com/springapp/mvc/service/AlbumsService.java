package com.springapp.mvc.service;

import com.springapp.mvc.models.Albums;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("albumService")
@Transactional
public class AlbumsService {

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    public List<Albums> getAll() {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM Albums");

        // Retrieve all
        return  query.list();
    }

    /**
     * Retrieves a single person
     */
    public Albums get( Integer id ) {
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Albums albums = (Albums) session.get(Albums.class, id);

        return albums;
    }
    /**
     * Adds a new person
     */
    public void add(Albums albums) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Save
        session.save(albums);
    }

    /**
     * Deletes an existing person
     * @param id the id of the existing person
     */
    public void delete(Integer id) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Albums albums = (Albums) session.get(Albums.class, id);

        // Delete
        session.delete(albums);
    }
}
