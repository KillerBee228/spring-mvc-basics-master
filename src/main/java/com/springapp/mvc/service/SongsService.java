package com.springapp.mvc.service;

import com.springapp.mvc.models.Songs;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service("songService")
@Transactional
public class SongsService {

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    public List<Songs> getAll() {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Create a Hibernate query (HQL)
        Query query = session.createQuery("FROM Songs");

        // Retrieve all
        return  query.list();
    }

    /**
     * Retrieves a single person
     */
    public Songs get( Integer id ) {
        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Songs songs = (Songs) session.get(Songs.class, id);

        return songs;
    }
    /**
     * Adds a new person
     */
    public void add(Songs songs) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Save
        session.save(songs);
    }

    /**
     * Deletes an existing person
     * @param id the id of the existing person
     */
    public void delete(Integer id) {

        // Retrieve session from Hibernate
        Session session = sessionFactory.getCurrentSession();

        // Retrieve existing person first
        Songs songs = (Songs) session.get(Songs.class, id);

        // Delete
        session.delete(songs);
    }
}
