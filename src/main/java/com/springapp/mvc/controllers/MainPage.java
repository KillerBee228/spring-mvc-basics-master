package com.springapp.mvc.controllers;


import com.springapp.mvc.models.Albums;
import com.springapp.mvc.models.Contacts;
import com.springapp.mvc.models.Singers;
import com.springapp.mvc.models.Songs;
import com.springapp.mvc.service.AlbumsService;
import com.springapp.mvc.service.ContactsService;
import com.springapp.mvc.service.SingersService;
import com.springapp.mvc.service.SongsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

@Controller
public class MainPage {

    @Resource(name = "singersService")
    private SingersService singersService;

    @Resource(name = "albumsService")
    private AlbumsService albumsService;

    @Resource(name = "songsService")
    private SongsService songsService;

    @Resource(name = "contactsService")
    private ContactsService contactsService;

    @RequestMapping(value = "/music",method = RequestMethod.GET)
    public String printWelcome(ModelMap model) {

        String main="";
        List<Singers> singerses = singersService.getAll();
        List <Albums> albumses = albumsService.getAll();
        List <Songs> songses = songsService.getAll();
        for(Singers singer : singerses){
            main+="•"+singer.getName()+"<BR>";
            for (Albums album : albumses){
                if(singer.getId()==album.getIdsinger()){
                    main+="••"+album.getName()+"<BR>";
                    for(Songs song : songses){
                        if(song.getIdalbum()==album.getId()){
                            main+="•••"+song.getName()+"<BR>";
                        }
                    }
                }
            }
            main+="__________________________ <BR>";
        }
        model.addAttribute("main",main);
        return "musicmain"; // view id
    }

    @RequestMapping(value = "/music/admin/singer/add",method = RequestMethod.GET)
    public String addSinger(@RequestParam("name") String name, ModelMap model) {
        Singers singers = new Singers();
        singers.setName(name);
        try {
            singersService.add(singers);
            model.addAttribute("state","Success");
        }catch (Exception e){
            model.addAttribute("state","Fail");
            return "redirect:";
        }
        return "redirect:";

    }

    @RequestMapping(value = "/music/admin/song/add",method = RequestMethod.GET)
    public String addSong(@RequestParam("name") String name,@RequestParam("idalbum") int idalbum, ModelMap model) {

        Songs songs = new Songs();
        songs.setName(name);
        songs.setIdalbum(idalbum);
        try {
            songsService.add(songs);
            model.addAttribute("state","Success");
        }catch (Exception e){
            model.addAttribute("state","Fail");
            return "redirect:";
        }
        return "redirect:";

    }

    @RequestMapping(value = "/music/admin/album/add",method = RequestMethod.GET)
    public String addContact(@RequestParam("name") String name,@RequestParam("idsinger") int idsinger, ModelMap model) {

        Albums albums = new Albums();
        albums.setName(name);
        albums.setIdsinger(idsinger);
        try {
            albumsService.add(albums);
            model.addAttribute("state","Success");
        }catch (Exception e){
            model.addAttribute("state","Fail");
            return "redirect:";
        }
        return "redirect:";
    }

    @RequestMapping(value = "/music/admin",method = RequestMethod.GET)
    public String adminPage(ModelMap model) {
        return "admin"; // view id
    }

    @RequestMapping(value = "/music/admin/song",method = RequestMethod.GET)
    public String addSongPage(ModelMap model) {

        Collection<Albums> albumses = albumsService.getAll();
        String listalbums="<td><select size="+ albumses.size()+" multiple name=\"idalbum\"><option disabled>Choose album</option>";
        for(Albums album : albumses){
            listalbums+="<option value="+album.getId()+">"+album.getName()+"</option>";
        }
        listalbums+="</select></td><BR><BR>";
        model.addAttribute("albums",listalbums);
        return "addsong"; // view id
    }

    @RequestMapping(value = "/music/admin/singer",method = RequestMethod.GET)
    public String addSingerPage(ModelMap model) {
        return "addsinger"; // view id
    }

    @RequestMapping(value = "/music/admin/album",method = RequestMethod.GET)
    public String addAlbumPage(ModelMap model) {

        Collection<Singers> singerses = singersService.getAll();
        String listsingers="<td><select size="+ singerses.size()+" multiple name=\"idsinger\"><option disabled>Choose singer</option>";
        for(Singers singer : singerses){
            listsingers+="<option value="+singer.getId()+">"+singer.getName()+"</option>";
        }
        listsingers+="</select></td><BR><BR>";
        model.addAttribute("singers",listsingers);
        return "addalbum"; // view id
    }

    @RequestMapping(value = "/phonebook",method = RequestMethod.GET)
    public String printWelcomePhone(ModelMap model) {

        Collection <Contacts> all = contactsService.getAll();
        String users="";
        for(Contacts contacts :all){
            users=users+ contacts.getName()+"		"+ contacts.getPhone()+"		<a href="+"/phonebook/del?id="+ contacts.getId()+">Delete</a>"+"<BR>";
        }
        System.out.println(users);
        model.addAttribute("users",users);
        return "hello"; // view id
    }

    @RequestMapping(value = "/phonebook/del",method = RequestMethod.GET)
    public String delContact(@RequestParam("id") int id, ModelMap model){

        contactsService.delete(id);
        return "redirect:";

    }

    @RequestMapping(value = "/phonebook/add",method = RequestMethod.GET)
    public String addContact(@RequestParam("name") String name,@RequestParam("phone") String phone, ModelMap model) {

        Contacts contacts = new Contacts();
        contacts.setName(name);
        contacts.setPhone(phone);
        contactsService.add(contacts);
        return "redirect:";

    }
}
