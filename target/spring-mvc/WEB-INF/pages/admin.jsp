<%--
  Created by IntelliJ IDEA.
  User: Роман
  Date: 10.03.2016
  Time: 23:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Music</title>
</head>
<body>
<center>
  <form action="/music/admin/singers">
    <button type="submit">Add Singer</button>
  </form>
  <form action="/music/admin/albums">
    <button type="submit">Add Album</button>
  </form>
  <form action="/music/admin/songs">
    <button type="submit">Add Song</button>
  </form>
</center>
<center>
  <form action="/music">
    <button type="submit">Back</button>
  </form>
</center>
</body>
</html>
