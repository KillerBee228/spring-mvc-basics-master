<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Album</title>
</head>
<body>
<center>
  <h1>Hello, here you can add new albums!</h1>
  <form method="get" action="/music/admin/albums/add">
    ${singers}
    <tr>
      <td align="right" valign="top">Name</td>
      <td><input type="text" name="name" size="25"></td>
    </tr>
    <tr>
      <td align="right" colspan="2">
        <input type="submit" value="Add">
        <input type="reset" name="reset" value="Reset">
      </td>
    </tr>
  </form>
  <h3>${state}<BR></h3>
  <form action="/music">
    <button type="submit">Back</button>
  </form>
</center>
</body>
</html>
